var reloj={
    horas:0,
    minutos:0,
    segundos:0,
    idDestino:"reloj",

    mostrar:function()
    {
        if (reloj.segundos>0 || reloj.minutos>0 || reloj.horas>0)
        {
            reloj.segundos--;
            if (reloj.segundos < 0)
            {
                reloj.segundos=59;
                reloj.minutos--;
            }
            if (reloj.minutos < 0)
            {
                reloj.minutos=59;
                reloj.horas--;
            }

            var string=reloj.doscaracteres(reloj.horas)+':'+reloj.doscaracteres(reloj.minutos)+':'+reloj.doscaracteres(reloj.segundos);
            document.getElementById(reloj.idDestino).innerHTML = string;
        }
    },
    
    mi_alerta:function()
	{
    alert ("Se ha acabado el tiempo" );
    },
    
    iniciar:function()
    {
        setInterval(reloj.mostrar, 1000);
    },
    
    doscaracteres:function(numero)
    {
        if(String(numero).length==1)
            return "0"+numero;
        return numero;
    },
}