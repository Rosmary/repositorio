require 'rho/rhocontroller'
require 'helpers/browser_helper'




class SiacoController < Rho::RhoController
  include BrowserHelper

  def index
    render :action => :index
  end
  
  def  do_regis
        render :action => :r_consumidor
   end 
   
  def unefa_calculo1
     render :action => :unefa_calculo1
   end  
   
  def ucla_calculo2
     render :action => :ucla_calculo2
   end  
   
   def ucla_organizacion
     render :action => :ucla_organizacion
   end
   
   def ucla_cursos
     render :action => :ucla_cursos
   end
  def ucla_cursos_publicador1
    render :action => :ucla_cursos_publicador1
  end
  
  def ucla_calculo2_publicador1
     render :action => :ucla_calculo2_publicador1
   end
  
  def unefa_cursos_publicador1
    render :action => :unefa_cursos_publicador1
  end
   
   def prueba
     render :action => :prueba
   end
   
   def busqueda
     render :action => :busqueda
   end
   
   def menu_organizaciones1
     render :action => :menu_organizaciones1
   end
   
   def menu_organizaciones2
     render :action => :menu_organizaciones2
   end
   
   def andina
         redirect :action => :c_andina
       end
     
     def lista_cocina
       redirect :action => :l_cocina
     end
     def lista_cursosm
        redirect :action => :c_matematica
      end
      
     def recetas
       redirect :action => :l_recetasn
     end
  
  def menu
    
    query = "servicioSolicitado=8&antiguedad=1"
    @bodyresjson = query_json(query)
    
    if @bodyresjson["exito"].to_s == "1"
      $session[:cantidad_publicaciones] = @bodyresjson["cantidadPublicaciones"]
    end
    
    if ($session[:rol])
      case $session[:rol]
        when "administrador"
          render :action => :menu_admin #administrador
        when "junta"
          render :action => :menu_junta #junta
        when "propietario"
          render :action => :menu_propietario #propietario
        when "empleado"
          render :action => :menu_empleado #empleado
        else
          render :action => :index
      end
    else
      render :action => :agenda
    end
  end
  
  def crear_publicacion
    json = '{"tipoPublicacionId":"1", "usuarioEmisorId": "4", "usuarioReceptorId": "6", "codigoCartelera": "CART12", "titulo": "'
    json += @params["asunto"].to_s
    json += '", "descripcion": "'
    json += @params["descripcion"].to_s
    json += '", "fecha": "'
    json += Time.now.strftime("%Y-%m-%d")
    json += '", "archivo": "Archivo en Binario"}'
    
    query = "servicioSolicitado=17&datos="+json 
    @bodyresjson = query_json(query)

    if @bodyresjson["exito"].to_s == "1"
      @msg = "Se ha escrito su publicaci&oacute;n"
    else
      @msg = "Hubo un error al procesar su publicaci&oacute;n"
    end
    
    @titulo = "ESTADO DE PUBLICACI&Oacute;N"
    
    render :action => :pantalla_mensajes
  end
  
  def query_json(query)
    @server = Rho::RhoConfig.RESTFUL_URL+query
    @response = Rho::AsyncHttp.get(:url =>@server, :headers => {"Content-Type" => "application/json" } ) 
    @bodyresponse = @response["body"]
    return Rho::JSON.parse(@bodyresponse) 
  end
  
  def consultar
     render :action => :autorizacion
     end
   
  def procesar_pago
    query = "servicioSolicitado=6&nroCuentaDepositante="+@params["nro-cuenta"].to_s +
      "&cod_inmueble="+$session[:id_inmueble].to_s+"&monto="+@params["monto"].to_s+"&rifCondominio="+@params["rif"].to_s +
        "&nroCuentaBeneficiario="+@params["nroCuentaBeneficiario"]
    @bodyresjson = query_json(query)
      
    if @bodyresjson["exito"].to_s() == "1"
      @msg = "Transferencia exitosa<br><br> " + "Observación:" +@bodyresjson["observacion"]
    else
      @msg = @bodyresjson["observacion"]
    end
    
    @titulo = "TRANSFERENCIA REALIZADA"
    render :action => :pantalla_mensajes
  end
    
  def get_areas_comunes
    query = "servicioSolicitado=7&idCondominio="+$session[:condominio_id].to_s
    @bodyresjson = query_json(query)
      
    if @bodyresjson["exito"].to_s() == "1"
      $session[:areascomunes] = @bodyresjson["datosAreas"]
    end
  end
  
  def ver_reservaciones

    for area in $session[:areascomunes]
      if area["id"]==@params["id"]
        @reservaciones = area["Reservaciones"]
        @nombre_area = area["nombre"]
        break
      end
    end
    
    render :action => :ver_reservaciones
  end
  
  def logout
    @msg = "Has cerrado la sesi&oacute;n"
    $session.clear
    index
  end
  
  def configuracion
    @msg = @params['msg']
    if $session[:rol]
      render :action => :configuracion
    else
      render :action => :index
    end
  end
  
  def vreg_noticia
    redirect :action => :r_noticia
  end
  
  
  def vreg_inmueble
    render :action => :r_inmueble
  end
  
  def vreg_propietario
    render :action => :r_propietario
  end
  
  def vreg_estacionamiento
    render :action => :r_estacionamiento
  end
  
  def vreg_empleado
    render :action => :r_empleado
  end
  
  def vreg_tnivelmp
    render :action => :r_tipo_empleado
  end
  
  def vreg_requisicion
    render :action => :r_requisicion
  end
  
  def vreg_sancion
    render :action => :r_sancion
  end
  
  def vreg_pago_sancion
    render :action => :r_pago_sancion
  end
  
  def vreg_lista_sancion
    render :action => :r_pago_sancion
  end
  
  def vreg_servicio_comun
    render :action => :r_servicio_comun
  end
  
  def vreg_area_comun
    render :action => :r_area_comun
  end
  def vreg_producto
    render :action => :r_producto
  end
  
  def vreg_pago_servicio_comun
    render :action => :r_pago_servicio_comun
  end
  
  def vreg_factura_producto
    render :action => :r_factura_producto
  end
  
  def vreg_proveedor
    render :action => :r_proveedor
  end

  def vreg_ingreso
    render :action => :r_ingreso
  end
  
  def vreg_tipo_producto
    render :action => :r_tipo_producto
  end
  
  def vreg_tipo_sancion
    render :action => :r_tipo_sancion
  end
  
  def vreg_tipo_inmueble
    render :action => :r_tipo_inmueble
  end
  
  def vreg_reserva_ac
    get_areas_comunes
    render :action => :reserva_ac
  end
  
  def vreg_sugerencia
    render :action => :sugerencia
  end
  
  def vreg_condominio
    render :action => :r_condominio
  end
  
  def vreg_egreso
    render :action => :r_egreso
  end
  
  def vreg_agenda
    render :action => :agenda
  end
  
  def vreg_novedad
    render :action => :r_novedad
  end
  
  def cartelera
    query = "servicioSolicitado=8&antiguedad=50"
    @bodyresjson = query_json(query)
    
    if @bodyresjson["exito"].to_s == "1" 
      $session[:publicaciones] = @bodyresjson["publicaciones"]
    end

    render :action => :cartelera
  end
  
  def vreg_solicitar_constancia
    render :action => :solicitar_constancia
  end
  
  def autorizaciones
    render :action => :autorizacion
  end
end