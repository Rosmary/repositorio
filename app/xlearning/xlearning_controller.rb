require 'rho/rhocontroller'
require 'helpers/browser_helper'




class XlearningController < Rho::RhoController
  include BrowserHelper

  def index
    render :action => :index
  end
  
  def solicitar_servicio
     render :action => :solicitar_servicio
   end
  
  def  do_regis
        render :action => :r_consumidor
   end 
   
  def unefa_calculo1
     render :action => :unefa_calculo1
   end  
   
  def ucla_calculo2
     render :action => :ucla_calculo2
   end  
   
   def ucla_organizacion
     render :action => :ucla_organizacion
   end
   
   def ucla_cursos
     render :action => :ucla_cursos
   end
   
  def ucla_cursos_publicador1
    render :action => :ucla_cursos_publicador1
  end
  
  def ucla_calculo2_publicador1
     render :action => :ucla_calculo2_publicador1
   end
  
  def unefa_cursos_publicador1
    render :action => :unefa_cursos_publicador1
  end
   
   def prueba
     render :action => :prueba
   end
   
   def busqueda
     render :action => :busqueda
   end
   
   def menu_organizaciones1
     render :action => :menu_organizaciones
   end
   
   def menu_organizaciones2
     render :action => :menu_organizaciones2
   end
  
     def lista_cursosm
        redirect :action => :c_matematica
      end


  def  archivos
     redirect :action => :archivos_cargados
   end   
   
  def   menu_administrador
     redirect :action => :menu_administrador
   end
 
    def logout
    @msg = "Has cerrado la sesi&oacute;n"
    
    index
  end

end